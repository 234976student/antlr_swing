package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	GlobalSymbols Global = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }
    
    public void NewVariable(String text) {
    	Global.newSymbol(text);
    }
    
    public void SetVariable(String text, Integer value) {
    	Global.setSymbol(text, value);
    }
    
    public Integer GetVariable(String text) {
    	return Global.getSymbol(text);
    }
    
    protected Integer getPower(Integer base, Integer power)
    {
		return (int)Math.pow(base, power);    	
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
}
