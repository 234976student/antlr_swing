tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (
        ^(PRINT e=expr) {drukuj ($e.out.toString());}
        | t=test)* ;
        
test    : ^(VAR ID)                {NewVariable ($ID.text);}
        | ^(PODST ID e1=expr)      {SetVariable ($ID.text, $e1.out);}
        ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr)               {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr)               {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr)               {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr)               {$out = $e1.out / $e2.out;}
        | ^(TEST_LESS_EQUAL     e1=expr e2=expr) {$out = $e1.out <= $e2.out ? 1 : 0;}
        | ^(TEST_LESS           e1=expr e2=expr) {$out = $e1.out < $e2.out ? 1 : 0;}
        | ^(TEST_GREATER_EQUAL  e1=expr e2=expr) {$out = $e1.out >= $e2.out ? 1 : 0;}
        | ^(TEST_GREATER        e1=expr e2=expr) {$out = $e1.out > $e2.out ? 1 : 0;}
        | ^(TEST_EQUAL          e1=expr e2=expr) {$out = $e1.out == $e2.out ? 1 : 0;}
        | ^(TEST_NOT_EQUAL      e1=expr e2=expr) {$out = $e1.out != $e2.out ? 1 : 0;}
        | ^(TEST_POWER          e1=expr e2=expr) {$out = getPower($e1.out, $e2.out);}
        | ID                                     {$out = GetVariable($ID.text);}
        | INT                                    {$out = getInt($INT.text);}
        ;
        
