grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

stat : expr NL -> expr

    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)    
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | PRINT expr NL -> ^(PRINT expr)
    | NL ->
    ;
    
expr : addExpr
    ( TEST_LESS_EQUAL^ addExpr
    | TEST_LESS^ addExpr
    | TEST_GREATER_EQUAL^ addExpr
    | TEST_GREATER^ addExpr
    | TEST_EQUAL^ addExpr
    | TEST_NOT_EQUAL^ addExpr
    )*
    ;
    
addExpr 
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : powExpr
      ( MUL^ powExpr
      | DIV^ powExpr
      )*
    ;

powExpr
    : atom
    ( TEST_POWER^ atom    
    )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

PRINT : 'print' ;

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


TEST_LESS_EQUAL : '<=';

TEST_LESS : '<';

TEST_GREATER_EQUAL : '>=';

TEST_GREATER : '>';

TEST_EQUAL : '==';

TEST_NOT_EQUAL : '!=';

TEST_POWER : '**' ;

LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
